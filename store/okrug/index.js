export const actions = {
    async fetchOkrugList(_, { params }) {
        const { data } = await this.$axios.get(`/common/okrug/list`, {
            params
        });
        return data || {};
    },
    async fetchOkrugDetail(_, id) {
        const { data } = await this.$axios.get(`/common/okrug/${id}/`);
        return data;
    },
    async getRegionList() {
        let res = await this.$axios.get(`/common/region/list/`)
        return res && res.data
    },
    async getDistrictByRegion(_, id) {
        let res = await this.$axios.get(`/common/region/detail/${id}/`)
        return res && res.data
    },
};