export const state = () => ({

});

export const getters = {};

export const actions = {
    async getContactsInfo(_, payload) {
        let contact = await this.$axios.get(`/cms/contact/${payload}`)
        return contact && contact.data || {}
    },
}