export const actions = {
  async fetchInformation() {
    const { data } = await this.$axios.get("/account/user/me/");
    return data;
  },
  async fetchAppealsList(_, params) {
    const { data } = await this.$axios.get(`/appeals/list/me/`, {
      params
    });
    return data;
  },
  async getAppealDetail(_, id) {
    const { data } = await this.$axios.get(`/appeals/detail/user/me/${id}/`);
    return data;
  }
};
