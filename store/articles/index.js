export const actions = {
  async fethcArticles(_, params) {
    const {
      data
    } = await this.$axios.get(`/structure/deputy/article/list/`, params);
    return data && data.results;
  },
  async fetchArticlesHome(_, id) {
    const {
      data
    } = await this.$axios.get(`/structure/deputy/article/${id}/detail`);
    return data
  }
};
