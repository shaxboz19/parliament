export const actions = {
    async getCategories() {
        const { data } = await this.$axios.get(`/structure/orderprojectcategory/list/`);
        return data;
    },
    async getRegulationCounts() {
        const { data } = await this.$axios.get(`/structure/orderproject/count/list/`);
        return data;
    },
    async getCategoryRegulations(_, payload) {
        const { data } = await this.$axios.get(`/structure/orderprojectcategory/detail/${payload}/`);
        return data;
    },
    async getRegulations(_, { params }) {
        let { data } = await this.$axios.get(`/structure/orderproject/list/`, {
            params
        });
        return data;
    },
    async getRegulationDetail(_, payload) {
        const { data } = await this.$axios.get(`/structure/orderproject/detail/${payload}/`);
        return data;
    },
    async getInitiatorsList() {
        const { data } = await this.$axios.get(`/structure/orderprojectinitiator/list/`);
        return data;
    },
    async getProjectTypeList() {
        const { data } = await this.$axios.get(`/structure/orderprojecttype/list/`);
        return data;
    },
};