export const state = () => ({
  sms_token: "",
  access_token: "",
  refresh_token: "",
  form: null,
  token: null,
  resetToken: null,
  resetUsername: null,
  resetUserData: null,
  isRegistrationPopup: false,
  isLoginPopup: false,
  isConfirmPopup: false,
  isPhonePopup: false,
  isRecoveryPopup: false,
  isConfirmResetCode: false,
  isProfile: false
});

export const mutations = {
  setForm(state, form) {
    state.form = { ...form, phone: form.username };
  },
  setToken(state, payload) {
    state.token = payload.token;
  },
  setLoginTokens(state, { access_token, refresh_token }) {
    state.access_token = access_token;
    state.refresh_token = refresh_token;
  },
  clearTokens(state) {
    state.access_token = null;
    state.refresh_token = null;
  },
  setRegistrationPopup(state, payload) {
    state.isRegistrationPopup = payload;
  },
  setLoginPopup(state, payload) {
    state.isLoginPopup = payload.value;
    state.isProfile = payload.isProfile;
  },
  setConfirmPopup(state, payload) {
    state.isConfirmPopup = payload;
  },
  setPhonePopup(state, payload) {
    state.isPhonePopup = payload;
  },
  setRecoveryPopup(state, payload) {
    state.isRecoveryPopup = payload;
  },
  setConfirmResetCode(state, payload) {
    state.isConfirmResetCode = payload;
  },
  closePopups(state) {
    state.isRegistrationPopup = false;
    state.isLoginPopup = false;
    state.isConfirmPopup = false;
    state.isPhonePopup = false;
    state.isRecoveryPopup = false;
    state.isConfirmResetCode = false;
  },
  setResetToken(state, payload) {
    state.resetToken = payload.token;
    state.resetUsername = payload.username;
  },
  setResetCode(state, { data }) {
    state.resetUserData = data;
    state.resetToken = null;
  }
};

export const actions = {
  // first step of registration
  async getSmsCode({ commit }, payload) {
    const { data } = await this.$axios.post(
      "/account/user/sms-authentification/",
      {
        username: payload.username
      }
    );
    commit("setForm", payload);
    commit("setToken", data);
    return data;
  },
  // second step of registration
  async sendSmsCode({ state, commit }, code) {
    const { data } = await this.$axios.post("/account/user/me/create/", {
      ...state.form,
      token: state.token,
      sms_code: code
    });
    commit("setLoginTokens", {
      access_token: data.access_token,
      refresh_token: data.refresh_token
    });
    localStorage.setItem("access_token", data.access_token);
    localStorage.setItem("refresh_token", data.refresh_token);
  },
  async sendResetCode({ state, commit }, { sms_code }) {
    const res = await this.$axios.post(
      "/account/user/check/reset-password/phone/verification/",
      {
        token: state.resetToken,
        sms_code
      }
    );
    if (res.status == 200 || res.status == 201) {
      commit("setResetCode", {
        data
      });
    }
    return res;
  },
  // login
  async login({ commit }, payload) {
    const { data } = await this.$axios.post("/account/user/login/", payload);
    commit("setLoginTokens", {
      access_token: data.access_token,
      refresh_token: data.refresh_token
    });
    localStorage.setItem("access_token", data.access_token);
    localStorage.setItem("refresh_token", data.refresh_token);
    return data;
  },
  // refresh token
  async refresh() {
    const refresh = localStorage.getItem("refresh_token");
    await this.$axios.post("", refresh);
  },
  // resetPassword
  async ResetPassword({ commit }, payload) {
    try {
      const res = await this.$axios.post(
        "/account/user/reset-password/phone/verification/",
        {
          username: payload
        }
      );
      if (res.status == 200 || res.status == 201) {
        commit("setResetToken", {
          username: payload,
          token: res.data.token
        });
      }
      return res;
    } catch (e) {
      console.error(e);
    }
    return;
  },
  async ConfirmReset({ state, dispatch }, password) {
    const res = await this.$axios.post("/account/user/reset-password/", {
      password: password,
      code: state.resetUserData.code,
      token: state.resetUserData.token,
      username: state.resetUsername.replace("+", "")
    });
    if (res.status == 200 || res.status == 201) {
      dispatch("login", {
        username: state.resetUsername.replace("+", ""),
        password: password
      });
    }
  }
};

export const getters = {
  getAccessToken: state => state.access_token,
  getRefreshToken: state => state.refresh_token,
  getRegistrationPopup: state => state.isRegistrationPopup,
  getLoginPopup: state => state.isLoginPopup,
  getConfirmPopup: state => state.isConfirmPopup,
  getPhonePopup: state => state.isPhonePopup,
  getRecoveryPopup: state => state.isRecoveryPopup,
  getConfirmResetCode: state => state.isConfirmResetCode,
  getIsProfile: state => state.isProfile
};
