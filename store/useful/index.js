export const state = () => ({

});

export const getters = {};

export const actions = {
    async getUsefulList() {
        let useful = await this.$axios.get(`/common/useful-links/list/`);
        return useful && useful.data
    },

}