export const actions = {
    async getFriendshipList() {
        let friendship = await this.$axios.get("/structure/deputyfriendship/list/");
        return (friendship && friendship.data) || {};
    },
    async getFriendshipDetail(_, id) {
        const { data } = await this.$axios.get(
            `/structure/deputyfriendship/detail/${id}/`
        );
        return data;
    }
};