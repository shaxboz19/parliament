export const state = () => ({

});

export const getters = {};

export const actions = {
    async getPhotosList(_, { params }) {
        let photos = await this.$axios.get(`/common/photo-gallery/list`, { params })
        return { results: photos && photos.data && photos.data.results, count: photos && photos.data && photos.data.count } || photos && photos.data || []
    },
    async getAllPhotos(_, payload) {
        let photos = await this.$axios.get(`/common/${payload}/photos`)
        return photos && photos.data || {}
    },


    async getVideosList(_, { params }) {
        let videos = await this.$axios.get('/common/video-gallery/list', { params })
        return {
            results: videos && videos.data && videos.data.results, count: videos && videos.data && videos.data.count
        } || videos && videos.data || []
    },
    async getAllVideos(_, payload) {
        let videos = await this.$axios.get(`/common/video-gallery/detail/${payload}`)
        return videos && videos.data || {}
    },

}