export const state = () => ({
 
    menu: []
});

export const mutations = {
    setMenu(state, menu) {
        state.menu = menu
    }
}
export const actions = {
    async GetMenu() {
        let menu = await this.$axios.get('/cms/menu/active/list/');
        return menu && menu.data || []
    }
}
export const getters = {
};
