export const state = () => ({});

export const getters = {};

export const actions = {
  async fetchDeputiesList(_, { params }) {
    let deputies = await this.$axios.get(`/structure/deputy/list/`, { params });
    return (deputies && deputies.data) || {};
  },
  async GetDeputiesList() {
    let deputies = await this.$axios.get(`/structure/deputy/list/`);
    return (deputies && deputies.data) || {};
  },
  async GetDeputyDetail(_, payload) {
    let post = await this.$axios.get(`/structure/deputy/detail/${payload}/`);
    return (post && post.data) || {};
  },
  async GetManagmentList() {
    let deputies = await this.$axios.get(`/structure/management/`);
    return (deputies && deputies.data) || {};
  },
  async GetKengashList() {
    let deputies = await this.$axios.get(
      `/structure/council/members/list/site/`
    );
    return (deputies && deputies.data) || {};
  },

  async GetAlphabeticalList(_, { params }) {
    let alplabetical = await this.$axios.get(
      `/structure/deputy/alphabetical/`,
      { params }
    );
    return (alplabetical && alplabetical.data) || {};
  },

  async getRegionList() {
    let res = await this.$axios.get(`/common/region/list/`);
    return res && res.data;
  },
  async getDistrictByRegion(_, id) {
    let res = await this.$axios.get(`/common/region/detail/${id}/`);
    return res && res.data;
  }
};
