export const actions = {
  async fetchMembersList(_, params) {
    const { data } = await this.$axios.get("/structure/members/", params);
    return data;
  }
};
