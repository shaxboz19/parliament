export const actions = {
  async getEvents(_, params) {
    const { data } = await this.$axios.get("/cms/post/calendar/days/", {
      params
    });
    return data;
  }
};
