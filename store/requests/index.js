export const state = () => ({
  isRequestsPopup: false
});
export const mutations = {
  setRequestsPopup(state, payload) {
    state.isRequestsPopup = payload;
  },
  closePopup(state) {
    state.isRequestsPopup = false;
  }
};
export const actions = {
  async fetchAllRegions() {
    const { data } = await this.$axios.get(`/common/region/list/`);
    return data;
  },
  async fetchRegionDetail(_, id) {
    const { data } = await this.$axios.get(`/common/region/detail/${id}/`);
    return data;
  },
  async fetchStatistics() {
    const { data } = await this.$axios.get("/appeals/statistics/");
    return data;
  },
  async fetchResponceCode(_, code) {
    const { data } = await this.$axios.get(`/appeals/${code}/check/`);
    return data;
  },

  async fetchSpeakerAssistant() {
    const { data } = await this.$axios.get("/structure/management/");
    return data;
  },
  async fetchFractionsList() {
    const { data } = await this.$axios.get("/structure/faction/list/");
    return data;
  },
  async fetchCommissionsList() {
    const { data } = await this.$axios.get("/structure/commission/list/");
    return data;
  },
  async fetchCommitteList() {
    const { data } = await this.$axios.get("/structure/committee/list/");
    return data;
  },
  async fetchDeputies() {
    const { data } = await this.$axios.get("/structure/deputy/list/");
    return data;
  }
};
export const getters = {
  getRequestsPopup: state => state.isRequestsPopup
};
