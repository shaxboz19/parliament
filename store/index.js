export const actions = {
  async getCommitetList() {
    const { data } = await this.$axios.get("/structure/committee/list/");
    return data;
  },
  async getCommitetDetail(_, id) {
    const { data } = await this.$axios.get(
      `/structure/committee/${id}/deputy/list/`
    );
    return data;
  }
};
