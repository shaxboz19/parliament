export const state = () => ({});

export const getters = {};

export const actions = {
    async fetchDepartmentsList() {
        let departments = await this.$axios.get(`/structure/department/list/`);
        return (departments && departments.data) || {};
    },
};