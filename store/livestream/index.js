export const actions = {
  async fetchLiveStreamList(_, params) {
    const { data } = await this.$axios.get(`/structure/livestream/list/`, {
      params
    });
    return data;
  }
};
