export const actions = {
  async GetFractionList() {
    let fraction = await this.$axios.get(`/structure/faction/list/`);
    return (fraction && fraction.data) || {};
  },
  async fetchFractionDetail(_, id) {
    let { data } = await this.$axios.get(`/structure/faction/detail/${id}/`);
    return data;
  }
};
