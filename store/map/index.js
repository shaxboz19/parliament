export const actions = {
  async getRegionList(_) {
    let res = await this.$axios.get(`/common/region/list/`);
    return res && res.data;
  },
  async getDeputyList(_, params) {
    let res = await this.$axios.get("/structure/deputy/list/", {
      params
    });
    return res && res.data;
  },
  async getFractionList() {
    let res = await this.$axios.get("/structure/faction/list/");
    return res && res.data;
  },
  async getCommitteeList() {
    let res = await this.$axios.get("/structure/committee/list/");
    return res && res.data;
  },
  async getDeputiesByRegion(_, payload) {
    let res = await this.$axios.get(`/structure/region/${payload}/deputies/`);
    return res && res.data;
  },
  async getDeputiesByDistrict(_, payload) {
    let res = await this.$axios.get(`/structure/district/${payload}/deputies/`);
    return res && res.data;
  },
  async getDeputiesByFraction(_, payload) {
    let res = await this.$axios.get(
      `/structure/faction/${payload}/deputy/list/`
    );
    return res && res.data;
  },
  async getDeputiesByCommittee(_, payload) {
    let res = await this.$axios.get(
      `/structure/committee/${payload}/deputy/list/`
    );
    return res && res.data;
  },
  async getSubdistrictByDistrict(_, payload) {
    let res = await this.$axios.get(
      `/common/district/${payload}/sub-district/list/`
    );
    return res && res.data;
  },
  async getDeputiesBySubdistrict(_, payload) {
    let res = await this.$axios.get(
      `/structure/sub-district/${payload}/deputies/`
    );
    return res && res.data;
  },
  async getDistrictByRegion(_, id) {
    const { data } = await this.$axios.get(`/common/region/detail/${id}/`);
    return data;
  }
};
