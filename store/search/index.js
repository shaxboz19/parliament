export const actions = {
  async fetchSearchResult(_, { limit, offset, title }) {
    const { data } = await this.$axios.get(`/search/filter/`, {
      params: {
        query: title,
        limit,
        offset
      }
    });
    return data;
  }
};
