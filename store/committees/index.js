export const actions = {
  async getCommitteList() {
    let committee = await this.$axios.get("/structure/committee/list/");
    return (committee && committee.data) || {};
  },
  async getCommittetDetail(_, id) {
    const { data } = await this.$axios.get(
      `/structure/committee/detail/${id}/`
    );
    return data;
  }
};
