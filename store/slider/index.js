export const actions = {
    async getSliderList() {
        let sliders = await this.$axios.get(`/cms/post/on-slider/list/`)
        return sliders && sliders.data || []
    }
}