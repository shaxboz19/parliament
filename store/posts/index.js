// import axios from 'axios'

// let instance = axios.create({
//     baseURL: 'https://oliymajlisapi.mdg.uz/api/v1',
//     timeout: 60000
// });

export const state = () => ({});

export const getters = {};

export const actions = {
    async getNews() {
        let news = await this.$axios.get("/cms/menu/group/news/posts/");
        return (news && news.data) || [];
    },
    async getEvents() {
        let events = await this.$axios.get("/cms/menu/group/events/posts/");
        return (events && events.data) || [];
    },
    async getPostsListInSlug(_, { params, slug }) {
        let posts = await this.$axios.get(`/cms/menu/${slug}/posts/`, { params });
        return (posts && posts.data) || [];
    },
    async getPostsListInGroup(_, { params, group }) {
        let posts = await this.$axios.get(`/cms/menu/group/${group}/posts/`, {
            params
        });
        return (posts && posts.data) || [];
    },
    async getPostDetail(_, payload) {
        let post = await this.$axios.get(`/cms/post/${payload}`);
        return (post && post.data) || post;
    },
    async getPostDetailCustom(_, payload) {
        let post = await this.$api.get(`/cms/post/${payload}`);
        return (post && post.data) || post;
    },
    async getMenuParentsInSlug(_, payload) {
        let menu = await this.$axios.get(`/cms/menu/${payload}/parent/`);
        return (menu && menu.data) || {};
    },
    async getMenuParentsInGroup(_, payload) {
        let menu = await this.$axios.get(`/cms/menu/group/${payload}/parent/`);
        return (menu && menu.data) || {};
    },
    async getFraction() {
        const { data } = await this.$axios.get("/structure/faction/list/");
        return data;
    },
    async getCommittet() {
        const { data } = await this.$axios.get(`/structure/committee/list/`);
        return data;
    },
    async getCommissions() {
        const { data } = await this.$axios.get(`/structure/commission/list/`);
        return data;
    },
    async getPostsListInTags(_, { params, slug }) {
        let posts = await this.$axios.get(`/cms/post/tag/${slug}/list/`, { params });
        return (posts && posts.data) || [];
    },
    async getPostTagInfo(_, payload) {
        let post = await this.$axios.get(`/cms/post/tag/${payload}/`);
        return (post && post.data) || {};
    },
};