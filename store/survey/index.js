export const actions = {
  async fetchSurveyList() {
    const { data } = await this.$axios.get("/cms/survey/");
    return data;
  },
  async fetchResults(_, id) {
    const { data } = await this.$axios.get(`/cms/survey/${id}/`);
    return data;
  },
  async voteSurvey(_, payload) {
    await this.$axios.post("/cms/survey/result/create/", payload);
  }
};
