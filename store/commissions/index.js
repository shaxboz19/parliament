export const actions = {
  async fetchCommissionDetail(_, id) {
    const { data } = await this.$axios.get(
      `/structure/commission/detail/${id}/`
    );
    return data;
  }
};
