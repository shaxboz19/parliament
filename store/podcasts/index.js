export const actions = {
  async fetchPodcastsList() {
    const { data } = await this.$axios.get("/common/podcast/list/");
    return data;
  },
  async fetchPodcastsById(_, id) {
    const { data } = await this.$axios.get(`/common/podcast/detail/${id}/`);
    return data;
  }
};
