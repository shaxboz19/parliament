import ru from "./locale/ru.json";
import uz from "./locale/uz.json";
import oz from "./locale/oz.json";
import en from "./locale/en.json";
// import qr from "./locale/qr.json";

let ENV = {
    FAVICON_PATH: process.env.FAVICON_PATH,
    // API_URL: process.env.API_URL,`
    APP_FILE_URL: process.env.APP_FILE_URL,
    APP_BASE_URL: process.env.APP_BASE_URL,
    IMAGE_SRC_SET_PROXY: process.env.IMAGE_SRC_SET_PROXY,
    YANDEX_METRIKA: process.env.YANDEX_METRIKA,
    GOOGLE_ANALITICS: process.env.GOOGLE_ANALITICS,
    IMAGE_SIZES: [100, 200, 300, 400, 500, 800, 1200, 1920],
    CACHE_TIME: +process.env.CACHE_TIME,
    CACHE: process.env.CACHE ? process.env.CACHE == "true" : true,
    DEV_TOOLS: process.env.DEV_TOOLS ? process.env.DEV_TOOLS == "true" : true,
    SILENT: process.env.SILENT ? process.env.SILENT == "true" : false,
    PRODUCTION_TIP: process.env.PRODUCTION_TIP ?
        process.env.PRODUCTION_TIP == "true" :
        true,
    PERFOMANCE: process.env.PERFOMANCE ? process.env.PERFOMANCE == "true" : false,
    BASE: process.env.BASE,
    HOST: process.env.HOST,
    PORT: process.env.PORT,
    APP_BASE_IMG: {
        PATH: `${process.env.APP_BASE_URL}/logo_og.png`,
        WIDTH: "1200",
        HEIGHT: "630"
    },

    GOOGLE_CUSTOM_SEARH_KEY: process.env.GOOGLE_CUSTOM_SEARH_KEY,
    GOOGLE_CUSTOM_SEARH_CX_KEY: process.env.GOOGLE_CUSTOM_SEARH_CX_KEY,

    YANDEX_MAP_TOKEN: process.env.YANDEX_MAP_TOKEN
};

export default {
    link: [{ rel: "icon", type: "image/x-icon", href: "/favicon.ico" }],
    // Global page headers: https://go.nuxtjs.dev/config-head
    head: {
        title: "Олий Мажлис Конунчилик палатаси",
        htmlAttrs: {
            lang: "uz"
        },
        meta: [{
                charset: "utf-8"
            },
            // {
            //     name: "telegram:channel",
            //     content: "@yuz_official"
            // },
            {
                name: "viewport",
                content: "width=device-width, initial-scale=1"
            },
            {
                name: "google-site-verification",
                content: ENV.GOOGLE_SEARH_CONSOLE_KEY
            },
            {
                name: "canonical",
                content: ENV.APP_BASE_URL
            },
            {
                name: "theme-color",
                // content: "#2e2d2d",
                content: "#004387"
            },

            {
                hid: "og:title",
                property: "og:title",
                content: "ЎЗБЕКИСТОН РЕСПУБЛИКАСИ ОЛИЙ МАЖЛИСИ ҚОНУНЧИЛИК ПАЛАТАСИ"
            },
            {
                hid: "og:url",
                property: "og:url",
                content: ENV.APP_BASE_URL
            },
            {
                hid: "og:image",
                property: "og:image",
                content: ENV.APP_BASE_IMG.PATH
            },

            {
                hid: "og:image:secure_url",
                property: "og:image:secure_url",
                content: ENV.APP_BASE_IMG.PATH
            },

            {
                hid: "og:image:width",
                property: "og:image:width",
                content: ENV.APP_BASE_IMG.WIDTH
            },
            {
                hid: "og:image:height",
                property: "og:image:height",
                content: ENV.APP_BASE_IMG.HEIGHT
            },

            {
                hid: "og:type",
                property: "og:type",
                content: "website"
            },
            {
                hid: "og:site_name",
                property: "og:site_name",
                content: ENV.APP_BASE_URL
            },

            // Google / Schema.org markup:
            {
                hid: "name",
                itemprop: "name",
                content: ENV.APP_BASE_URL
            },
            {
                hid: "image",
                itemprop: "image",
                content: ENV.APP_BASE_IMG.PATH
            },

            {
                hid: "image:width",
                property: "image:width",
                content: ENV.APP_BASE_IMG.WIDTH
            },
            {
                hid: "image:height",
                property: "image:height",
                content: ENV.APP_BASE_IMG.HEIGHT
            }
        ],
        link: [{
            rel: "icon",
            type: "image/x-icon",
            href: ENV.FAVICON_PATH
        }],

        // Global CSS: https://go.nuxtjs.dev/config-css
        css: [
            "~/assets/css/swiper.css",
            "~/assets/fonts/fontawesome/css/font-awesome.min.css",
            "~/assets/fonts/Montserrat/stylesheet.css",
            "~/assets/fonts/Effra/stylesheet.css",
            "~/assets/fonts/icomoon/style.css",
            "~/assets/css/style.css",
            "~/assets/css/main.css",
            "~/assets/css/media.css"
        ],

        // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
        plugins: [
            { src: "~/plugins/VueAwesomeSwiper.js", mode: "client" },
            "~/plugins/axios.js",
            "~/plugins/share-network.js",
            "~/plugins/vue-print.js",
            { src: "~/plugins/vuelidate.js" },
            {
                src: "~/plugins/eventBus.js",
                ssr: false
            },
            { src: "./plugins/VueHighCharts.js", ssr: false }
        ],
    },

    // Global CSS: https://go.nuxtjs.dev/config-css
    css: [
        "~/assets/css/swiper.css",
        "~/assets/fonts/fontawesome/css/font-awesome.min.css",
        "~/assets/fonts/Montserrat/stylesheet.css",
        "~/assets/fonts/Effra/stylesheet.css",
        "~/assets/fonts/icomoon/style.css",
        "~/assets/css/style.css",
        "~/assets/css/main.css",
        "~/assets/css/media.css"
    ],

    // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
    plugins: [
        { src: "~/plugins/VueAwesomeSwiper.js", mode: "client" },
        "~/plugins/axios.js",
        "~/plugins/api.js",
        "~/plugins/share-network.js",
        "~/plugins/vue-print.js",
        { src: "~/plugins/vuelidate.js" },
        {
            src: "~/plugins/eventBus.js",
            ssr: false
        },
        { src: "~/plugins/aos.js", ssr: false },
        { src: "./plugins/VueFIlters.js" },
        {
            src: "~/plugins/i18n.js"
        },
        { src: "./plugins/VueMask.js" }
    ],

    loading: {
        color: "#0b4aa8",
        height: "3px"
    },
    // Auto import components: https://go.nuxtjs.dev/config-components
    components: true,

    // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
    buildModules: [],

    // Modules: https://go.nuxtjs.dev/config-modules
    modules: [
        // https://go.nuxtjs.dev/bootstrap
        "bootstrap-vue/nuxt",
        "nuxt-i18n",
        "@nuxtjs/axios",
        "vue-social-sharing/nuxt",
        "@nuxtjs/toast"
    ],
    toast: {
        duration: 3000,
        position: "top-center",
        register: [
            // Register custom toasts
            {
                name: "my-error",
                message: "Oops...Something went wrong",
                options: {
                    type: "error"
                }
            }
        ]
    },
    i18n: {
        locales: [{
                code: "uz",
                name: "O'zbek"
            },
            {
                code: "oz",
                name: "Ўзбек"
            },
            {
                code: "ru",
                name: "Русский"
            },

            {
                code: "en",
                name: "English"
            }
        ],
        strategy: "prefix_except_default",
        defaultLocale: "oz",
        vueI18n: {
            fallBackLocale: "uz",
            messages: {
                uz: uz,
                oz: oz,
                ru: ru,
                en: en
                    // qr: qr
            }
        }
    },

    axios: {
        baseUrl: process.env.VUE_APP_API_URL
    },

    // Build Configuration: https://go.nuxtjs.dev/config-build
    build: {}
};