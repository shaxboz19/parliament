export default ({
    app
}, inject) => {
    const {
        context: {
            $axios
        },
        i18n
    } = app;
    i18n.beforeLanguageSwitch = (_, newLocale) => {
        $axios.setHeader("Accept-Language", newLocale || "uz");
    }

    inject('imageProxy', (url, size) => `https://imageproxy.osg.uz/${size || 200}x/${url}`)

};
