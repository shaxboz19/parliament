// export default function({ $axios, app: { i18n, store } }) {
//     $axios.setHeader('Accept-Language', i18n.locale || 'oz');
//     $axios.onRequest(config => {
//         const storageToken = localStorage.getItem('access')
//         storageToken && $axios.setHeader(
//             "Authorization",
//             `Bearer ${storageToken}`
//         );
//         return config
//     })

//     $axios.onResponse(config => {
//         return config
//     })

//     $axios.onError(config => {
//         const code = parseInt(config.response && config.response.status)
//         if (code == 401) {
//             store.dispatch("auth/refresh")
//         }
//     })
// }

import axios from "axios";

axios.baseURL = process.env.VUE_APP_BASE_URL;

export default function({ $axios, redirect, store, i18n }) {
  $axios.setHeader("Accept-Language", i18n.locale || "oz");
  $axios.onRequest(config => {
    const storageToken = localStorage.getItem("access_token");
    $axios.setHeader(
      "Authorization",
      storageToken ? `Bearer ${storageToken}` : ""
    );
    return config;
  });

  $axios.onResponse(config => {
    return config;
  });

  $axios.onError(async error => {
    const code = parseInt(error.response && error.response.status);
    if (code === 400) {
      // redirect("/400");
      // } else if (code === 404) {
      //   redirect("/404");
    } else if (code === 401) {
      // error.response &&
      //   error.response.data &&
      //   error.response.data.detail ==
      //     "Given token not valid for any token type" &&
      //   store.dispatch("authantication/func401");
      localStorage.removeItem("access_token");

      redirect("/");
    }
    return error;
  });
}
